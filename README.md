# THIS IS A FORKED REPO
# LaunchDarkly SDK for Browser JavaScript

This is a necessary fork, due to incompatibility issues between launchdarkly and ionic/capacitor-http plugin.

The changes introduced in the azur sso implementation take advantage of the [capacitor-http plugin](https://capacitorjs.com/docs/v4/apis/http) which can be seen [here](https://gitlab.com/menlo79/wilson/-/blob/5f4ce288c007d5e503d1f1d5b43974fa2a6e7d0c/apps/mobile/ios/App/App/capacitor.config.json#L32-34).

This plugin in the currently used (4.8.x) & latest available version (5.3.2 as of writing) is not implementing the MDN standardized implementation of `getResponseHeader()` - see docs [here](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/getResponseHeader).

The search performed in that method should be **case-insensitive**. This is not the case as seen in the below posted links for android & ios implementations of capacitor http.

- android:
  - [native-bridge.js](https://github.com/ionic-team/capacitor/blob/5.2.3/android/capacitor/src/main/assets/native-bridge.js#L668)
  - [HttpRequestHandler.java](https://github.com/ionic-team/capacitor/blob/5.2.3/android/capacitor/src/main/java/com/getcapacitor/plugin/util/HttpRequestHandler.java#L211)
- ios:
  - [native-bridge.js](https://github.com/ionic-team/capacitor/blob/5.2.3/ios/Capacitor/Capacitor/assets/native-bridge.js#L668)
  - [HttpRequestHandler.swift](https://github.com/ionic-team/capacitor/blob/5.2.3/ios/Capacitor/Capacitor/Plugins/HttpRequestHandler.swift#L120)

Since the [LaunchDarkly sdk we are using](https://docs.launchdarkly.com/sdk/client-side/javascript) is built to be used in a browser environment it expects calls to the `getResponseHeader()` method to follow the MDN standards. Because of this it [calls that method with a lowercase string](https://github.com/launchdarkly/js-sdk-common/blob/5.0.3/src/Requestor.js#L55-L56) and based on that either continues with the flow or throws an error.

For us now though, since capacitor http is enabled & the standard `getResponseHeader()` implementation is replaced/overwritten by the capacitor-http implementation, when we call `getResponseHeader('content-type')` & the header has the key `'Content-Type'` - the method will return `undefined` -> resulting in an error being thrown and initialization of LD + fetching of LD-flags failing.

The quickest & easiest solution we could come up with for now was the following:

- Fork the `launchdarkly-js-client-sdk` repo into our gitlab
- Create a branch based on the `2.22.1` tag (currently used version by us)
- Modifiy the `getResponseHeader()` to `header()` mapping [here](https://github.com/launchdarkly/js-client-sdk/blob/2.22.1/src/httpRequest.js#L46)
  - instead of calling `getResponseHeader()` with a lowercase key - call it with a capitalized key
  - now when the `js-sdk-common` is executing that preivously mentioned code - [here](https://github.com/launchdarkly/js-sdk-common/blob/5.0.3/src/Requestor.js#L55-L56) the lowercase key will be capitalized and find the correct key - thus not resulting in an error being thrown and LD works
- Create an issue in the capacitor github repo describing the issue & providing a solution approach
- Wait for the fix to be implemented
- Update capacitor & replace this fork with the original launchdarkly-js-client-sdk

## Branch containing changes

- [custom2.22.1](https://gitlab.com/menlo79-public/launchdarkly-js-client-sdk-fork/-/tree/custom2.22.1) which is based on release tag `2.22.1`

## How to apply changes

- checkout branch `custom2.22.1`
- apply changes
- run `npm run build`
- commit both changed code & build output
- push changes to `custom2.22.1`
- switch to wilson repo
- checkout new branch based on `develop`
- run `npm uninstall launchdarkly-js-client-sdk`
- run `npm install 'git+https://gitlab.com/menlo79-public/launchdarkly-js-client-sdk-fork.git#custom2.22.1'`
- commit changes in `package-lock.json`
- push & create MR to `develop` branch

## How to deploy if changes were done

There seems to be wierd caching issue with native builds which would result in the updated package not being present in native builds, but present in web builds.
I found that the following procedure resulted in the native build having the correct & updated package. Feel free to point out the command I missed, mistake made during the update process which would prevent this from happening!

- navigate to wilson repo
- run `trash dist/*`
- run `trash apps/mobile/ios/App/App/public/*`
- run `nx run mobile:build:{#env}`
- run `nx run mobile:sync:{#target}`
- run `nx run mobile:open:{#target}`
- run `npm run deploy:mobile:{#env}:{#target}`

## Open Pull Request at capacitor http

- [https://github.com/ionic-team/capacitor/pull/6820](https://github.com/ionic-team/capacitor/pull/6820)
